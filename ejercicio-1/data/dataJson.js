const fs = require('fs');
const archivo ='./data/persona.json';

class DataPerson {

  constructor() {
    this.data = []
    this.leer = fs.readFileSync
    this.escribir = fs.writeFileSync
    this.cargarDatos()
  }

  cargarDatos = () => {
    if(!fs.existsSync(archivo) || this.data.length !== 0){
      return null;
    } else {
      let rawdata = this.leer(archivo);
      this.data = JSON.parse(rawdata);
    }
  }
  
  guardar = () => {  
    let data = JSON.stringify(this.data, null, 2);
    this.escribir(archivo, data);
  };

  addPersona = (persona) => {
    this.data.push(persona)
    this.guardar()
  }
  
  getDB = () => {
    return this.data
  };

  editPersona = (persona, index) => {
    //const index = this.data.findIndex(object => object.id === id);
    
    this.data[index].apellidos = persona.apellidos
    this.data[index].ci = persona.ci
    this.data[index].direccion = persona.direccion
    this.data[index].sexo = persona.sexo
    this.data[index].nombres = persona.nombres

    this.guardar();
    return this.data[index];
  }

  deletPersona = (id) => {
    let personaEliminada = this.data[id];
    this.data.splice(id, 1)

    this.guardar();
    return personaEliminada;
  }
}

module.exports = DataPerson
