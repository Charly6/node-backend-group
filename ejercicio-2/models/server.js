const express = require('express');

class Server {
  constructor(){
    this.app = express();
    this.port = process.env.PORT || 8080;
    this.personaPath = '/api/persona';
    this.middlewares();
    this.routes();
  }

  middlewares() {
    //Lectura y parseo del body
    //Cualquier informacion que venga se serializara en formato JSON
    this.app.use(express.json());
    //Se publica la carpeta public
    this.app.use(express.static('public'));
  }

  routes() {
    this.app.use(this.personaPath, require('../routes/persona'));
  }

  listen() {
    this.app.listen(this.port, () => {
      console.log('Servidor corriendo en el puerto', this.port);
    });
  }
}

module.exports = Server;
