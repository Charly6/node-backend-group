const { Router } = require('express');
const { 
  personaPost,
  personaGet,
  personaPut,
  personaDelete,
  personaGetCiSex,
  personaGetSexo
 } = require('../controller/persona');

const router = Router();

router.post('/', personaPost);
// router.get('/:nombre?/:apellido?', personaGet);
router.get('/:x/:sexo', personaGetSexo);
router.get('/:ciSex', personaGetCiSex);
router.get('/', personaGet);
router.put('/:id', personaPut);
router.delete('/:id', personaDelete);

module.exports = router;
