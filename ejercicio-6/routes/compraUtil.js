const { Router } = require('express');
const { 
  compraUtilPost,
  compraUtilGet,
  compraUtilPut,
  compraUtilDelete
 } = require('../controller/compraUtil');

const router = Router();

router.post('/', compraUtilPost);
router.get('/', compraUtilGet);
router.put('/:id', compraUtilPut);
router.delete('/:id', compraUtilDelete);

module.exports = router;
