const { v4: uuidv4 } = require('uuid');

class CompraUtil {
  id = '';
  material = {
    "c-1": "",
    "c-2": ""
  }
  precio_unitario = {
    "c-1": "",
    "c-2": ""
  }
  moneda = "";
  precio_total = "";

  constructor( material, precio_unitario, moneda ){
    this.id = uuidv4();
    this.material = material;
    this.precio_unitario = precio_unitario;
    this.moneda = moneda;
    this.precio_total = precio_unitario["c-1"] + precio_unitario["c-2"];
  }
}

module.exports = CompraUtil;
