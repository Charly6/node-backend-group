const fs = require('fs');
const archivo ='./data/delivery.json';

class DataDelivery {

  constructor() {
    this.data = []
    this.leer = fs.readFileSync
    this.escribir = fs.writeFileSync
    this.cargarDatos()
  }

  cargarDatos = () => {
    if(!fs.existsSync(archivo) || this.data.length !== 0){
      return null;
    } else {
      let rawdata = this.leer(archivo);
      this.data = JSON.parse(rawdata);
    }
  }
  
  guardar = () => {  
    let data = JSON.stringify(this.data, null, 2);
    this.escribir(archivo, data);
  };

  addDelivery = (delivery) => {
    this.data.push(delivery)
    this.guardar()
  }
  
  getDB = () => {
    return this.data
  };

  editDelivery = (delivery, index) => {
    //const index = this.data.findIndex(object => object.id === id);
    
    this.data[index].repartidor = delivery.repartidor
    this.data[index].cliente = delivery.cliente
    this.data[index].pedido = delivery.pedido
    this.data[index].direccion = delivery.direccion
    this.data[index].precio = delivery.precio
    this.data[index].precio_total = delivery.precio.refresco + delivery.precio.pizza
    this.data[index].moneda = delivery.moneda

    this.guardar();
    return this.data[index];
  }

  deletDelivery = (id) => {
    let deliveryEliminado = this.data[id];
    this.data.splice(id, 1)

    this.guardar();
    return deliveryEliminado;
  }
}

module.exports = DataDelivery
