const { response, request } = 'express';
const DataDelivery = require('../data/dataJson')
const Delivery = require('../models/delivery')

const dataDelivery = new DataDelivery()

//Get
const deliveryGet = (req, res = response) => {
  let data = dataDelivery.getDB()

  res.json({
    msg: 'Datos optenidos con exito',
    data
  });
};

//Put
const deliveryPut = (req = request, res = response) => {
  const { id } = req.params;

  const { repartidor, cliente, pedido, direccion, precio, moneda } = req.body;
  
  const index = dataDelivery.data.findIndex(object => object.id === id)

  if (index !== -1) {
    deliveryEditado = dataDelivery.editDelivery({ repartidor, cliente, pedido, direccion, precio, moneda }, index)
    res.json({
      msg: `Delivery con el id: ${id} se modifico`,
      deliveryEditado
    });
  }else {
    res.json({
      msg: `Delivery con el id: ${id} no se encontro`,
    });
  }

};

//Post
const deliveryPost = (req = request, res) => {
  const { repartidor, cliente, pedido, direccion, precio, moneda } = req.body;
  nuevoDelivery = new Delivery(repartidor, cliente, pedido, direccion, precio, moneda)
  dataDelivery.addDelivery(nuevoDelivery)

  res.status(201).json({
    msg: 'Delivery Agregado con exito',
    nuevoDelivery
  });
}

//Deleted
const deliveryDelete = (req, res = response) => {
  
  const { id } = req.params;

  const index = dataDelivery.data.findIndex(object => object.id === id)

  if (index !== -1) {
    let deliveryEliminado = dataDelivery.deletDelivery(index)
    res.json({
      msg: `Delivery con el id: ${id} se elimino`,
      deliveryEliminado
    });
  } else {
    res.json({
      msg: 'Delivery no encontrado'
    }); 
  }
  
};

module.exports = {
  deliveryPost,
  deliveryGet,
  deliveryPut,
  deliveryDelete
};
