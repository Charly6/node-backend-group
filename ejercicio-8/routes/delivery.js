const { Router } = require('express');
const { 
  deliveryPost,
  deliveryGet,
  deliveryPut,
  deliveryDelete
 } = require('../controller/delivery');

const router = Router();

router.post('/', deliveryPost);
router.get('/', deliveryGet);
router.put('/:id', deliveryPut);
router.delete('/:id', deliveryDelete);

module.exports = router;
