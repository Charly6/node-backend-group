const { v4: uuidv4 } = require('uuid');

class Delivery {
  id = '';
  repartidor = '';
  cliente = "";
  pedido = {};
  direccion = "";
  precio = {};
  moneda = ""

  constructor( repartidor, cliente, pedido, direccion, precio, moneda ){
    this.id = uuidv4();
    this.repartidor = repartidor;
    this.cliente = cliente;
    this.pedido = pedido;
    this.direccion = direccion;
    this.precio = precio;
    this.precio_total = precio.refresco + precio.pizza;
    this.moneda = moneda;
  }
}

module.exports = Delivery;
