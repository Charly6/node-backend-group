# Node Backend
***
## Table de Contenido
1. [Ejercicio 1](#ejercicio-1)
2. [Ejercicio 2](#ejercicio-2)
3. [Ejercicio 5](#ejercicio-3)
4. [Ejercicio 6](#ejercicio-4)
4. [Ejercicio 8](#ejercicio-8)

## Ejercicio 1
***
### Iniciar el ejercicio
1. Posicionarse en la carpeta del ejercicio en cmd.
2. Correr el comando: npm i
3. Correr el comando: node app

### Para testear el ejercicio
1. Desde Postman importar el archivo: ejercicio-1.postman_collection.json
2. Comenzar a usar los request establecidos.

## Ejercicio 2
***
### Iniciar el ejercicio
1. Posicionarse en la carpeta del ejercicio en cmd.
2. Correr el comando: npm i
3. Correr el comando: node app

### Para testear el ejercicio
1. Desde Postman importar el archivo: ejercicio-2.postman_collection.json
2. Comenzar a usar los request establecidos.

## Ejercicio 5
***
### Iniciar el ejercicio
1. Posicionarse en la carpeta del ejercicio en cmd.
2. Correr el comando: npm i
3. Correr el comando: node app

### Para testear el ejercicio
1. Desde Postman importar el archivo: ejercicio-5.postman_collection.json
2. Comenzar a usar los request establecidos.

## Ejercicio 6
***
### Iniciar el ejercicio
1. Posicionarse en la carpeta del ejercicio en cmd.
2. Correr el comando: npm i
3. Correr el comando: node app

### Para testear el ejercicio
1. Desde Postman importar el archivo: ejercicio-6.postman_collection.json
2. Comenzar a usar los request establecidos.

## Ejercicio 8
***
### Iniciar el ejercicio
1. Posicionarse en la carpeta del ejercicio en cmd.
2. Correr el comando: npm i
3. Correr el comando: node app

### Para testear el ejercicio
1. Desde Postman importar el archivo: ejercicio-8.postman_collection.json
2. Comenzar a usar los request establecidos.