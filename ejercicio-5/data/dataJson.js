class MyDateTime {

  constructor() {
    this.date;

    this.dias = ['Domingo', 'Lunes','Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado']
    this.meses = ['Enero', 'Febrero', 'Marzo' , 'Abril' , 'Mayo ', 'Junio', 'Julio', 'Agosto','Septiembre','Octubre','Noviembre','Diciembre']

  }

  get = () => {
    this.update();
    return this.Datetime
  }

  update = () => {
    this.date =  new Date();
    var horas = this.date.toLocaleTimeString()
    var ampm

    if(horas.split(' ').length == 2){
      let datas = horas.split(' ')
      horas = datas[0]
      ampm = datas[1]
    } else {
      if(horas >= 12){
        horas = horas - 12;
        ampm = 'PM'
      }else{
        ampm =  'AM'
      }
      if (horas == 0) {
        horas == 12
      }
    }

    this.Datetime = 
      {
        Day: this.dias[this.date.getDay()],
        Date_1: `${this.date.getDate()} de ${this.meses[this.date.getMonth()]} `,
        Year: this.date.getFullYear(),
        Date_2: `${this.date.getDate()} / ${this.date.getMonth()+1} / ${this.date.getFullYear()}`,
        hour: horas,
        Prefix: ampm
      }
  }
}

module.exports = MyDateTime
