const { Router } = require('express');
const { 
  dateTimeGet
 } = require('../controller/dateTime');

const router = Router();

router.get('/', dateTimeGet);

module.exports = router;
