const { response, request } = 'express';
const MyDateTime = require('../data/dataJson')

const dateTime = new MyDateTime()

//Get
const dateTimeGet = (req, res = response) => {
  let obj = dateTime.get()

  res.json(obj);
};

module.exports = {
  dateTimeGet
};
